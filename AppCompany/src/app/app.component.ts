import {Component, OnInit} from '@angular/core';
import {Company, virtualProcedure} from './Types/company.type';
import {ControlContainer, Form, FormControl, FormGroup, Validators} from '@angular/forms';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import {GenericService} from './http-services/generic.service';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css']
})
export class AppComponent implements OnInit {

  reactiveFormCompany: FormGroup;
  public company!: Company;


  constructor(private readonly genericService: GenericService<boolean, string>) {
    this.reactiveFormCompany = new FormGroup({});
  }


  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.reactiveFormCompany = new FormGroup({
      nit                      : new FormControl({value: ''}, Validators.required),
      idType                   : new FormControl({value: ''}, Validators.required),
      nameCompany              : new FormControl({value: ''}, Validators.required),
      firstName                : new FormControl({value: ''}, Validators.required),
      secondName               : new FormControl({value: ''}),
      firstLastName            : new FormControl({value: ''}, Validators.required),
      secondLastName           : new FormControl({value: ''}),
      email                    : new FormControl({value: ''}),
      authorizeSendMessagePhone: new FormControl({value: ''})
    });
  }

  public dataCompanyEvent(company: Company): void {
    this.company = {...company};
    this.reactiveFormCompany.get('nit')
      ?.setValue(company.nit);
    this.reactiveFormCompany.get('idType')
      ?.setValue(company.idType);
    this.reactiveFormCompany.get('nameCompany')
      ?.setValue(company.nameCompany);
    this.reactiveFormCompany.get('firstName')
      ?.setValue(company.firstName);
    this.reactiveFormCompany.get('secondName')
      ?.setValue(company.secondName);
    this.reactiveFormCompany.get('firstLastName')
      ?.setValue(company.firstLastName);
    this.reactiveFormCompany.get('secondLastName')
      ?.setValue(company.secondLastName);
    this.reactiveFormCompany.get('email')
      ?.setValue(company.email);
    this.reactiveFormCompany.get('authorizeSendMessagePhone')
      ?.setValue(company.authorizeSendMessagePhone);
    console.log(this.reactiveFormCompany.value);
  }

  public registerCompany(): void {
    this.genericService
      .post(`https://localhost:5001/VirtualRegisterCompany`,
        JSON.stringify({...this.reactiveFormCompany.value}))
      .subscribe(value => {
        console.log(value);
      });
  }
}
