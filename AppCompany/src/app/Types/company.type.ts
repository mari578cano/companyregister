export type Company = {
  id: number
  nit: number,
  idType: number,
  nameCompany: string,
  firstName: string,
  secondName: string,
  firstLastName: string,
  secondLastName: string,
  email: string,
  authorizeSendMessagePhone: boolean,
  authorizeSendMessageEmail: boolean,
  created: string,
  virtualProcedures: virtualProcedure

};

export type virtualProcedure = {
  id: number,
  companyId: number,
  dateAdded: string,
  isActive: boolean
};



