import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T, Z> {

  constructor(private readonly httpClient: HttpClient) {
  }

  public get(url: string): Observable<T> {
    return this.httpClient.get<T>(url);
  }

  public post(url: string, body: Z): Observable<T> {
    const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
    return this.httpClient.post<T>(url, body, {headers});
  }
}
