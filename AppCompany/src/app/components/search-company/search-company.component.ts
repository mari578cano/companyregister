import {Component, OnInit, ViewChild, ElementRef, Output, EventEmitter} from '@angular/core';
import {GenericService} from '../../http-services/generic.service';
import {Company} from '../../Types/company.type';
import {HttpEvent} from '@angular/common/http';

@Component({
  selector   : 'app-search-company',
  templateUrl: './search-company.component.html',
  styleUrls  : ['./search-company.component.css']
})
export class SearchCompanyComponent implements OnInit {

  @ViewChild('inputSearch')
  inputSearch!: ElementRef;
  @Output()
  dataCompanyEvent: EventEmitter<Company>;

  constructor(private readonly genericService: GenericService<Company, unknown>) {
    this.dataCompanyEvent = new EventEmitter<Company>();
  }

  ngOnInit(): void {
  }

  public searched(): void {
    const value = this.inputSearch.nativeElement.value;
    if (value) {

      this.genericService
        .get(`https://localhost:5001/Company/validate/${value}`)
        .subscribe((company: Company) => {
          if (company.id) {
            this.dataCompanyEvent.emit(company);
          } else {
            alert('sin datos');
          }
        });
    }
  }
}


