﻿using ServiceCompany.Entities;
using System.Threading.Tasks;

namespace ServiceCompany.IRepository
{
    public interface ICompanyRepository
    {
        Task<Company> ValidateCompany(long nit);
    }
}
