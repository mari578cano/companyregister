﻿using Microsoft.EntityFrameworkCore;
using ServiceCompany.Data;
using ServiceCompany.Entities;
using ServiceCompany.IRepository;
using System;
using System.Threading.Tasks;

namespace ServiceCompany.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly DataContext _context;

        public CompanyRepository(DataContext context) {
            _context = context;
        }

        public async Task<Company> ValidateCompany(long nit)
        {
            Company company = new Company();
            company = await _context.Company.Include(s => s.virtualProcedures).FirstOrDefaultAsync(c => c.NIT == nit);

            if (company.Id != 0 && company.NIT == nit) {
                return company;
            }

            return company;
        }
    }
}
