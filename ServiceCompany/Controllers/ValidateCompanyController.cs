﻿using Microsoft.AspNetCore.Mvc;
using ServiceCompany.Entities;
using ServiceCompany.IRepository;
using System.Threading.Tasks;

namespace ServiceCompany.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ValidateCompanyController : Controller
    {
        private readonly ICompanyRepository _company;

        public ValidateCompanyController(ICompanyRepository company) {
            _company = company;
        }

        [HttpGet("validate/{nit}")]
        public async Task<ActionResult<Company>> ValidateCompany(long nit) {
            Company companyResult = await _company.ValidateCompany(nit);

            if (companyResult.Id > 0)
                return Ok(companyResult);
            else
                return BadRequest(companyResult);
        }
    }
}
