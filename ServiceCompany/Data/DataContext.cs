﻿using Microsoft.EntityFrameworkCore;
using ServiceCompany.Entities;

namespace ServiceCompany.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Company> Company { get; set; }
        public DbSet<VirtualProceduresCompanies> virtualProceduresCompanies { get; set; }
    }
}
