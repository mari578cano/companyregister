﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceCompany.Entities
{
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public long NIT { get; set; }
        public int IdType { get; set; }
        public string NameCompany { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public bool AuthorizeSendMessagePhone { get; set; }
        public bool AuthorizeSendMessageEmail { get; set; }
        public DateTime Created { get; set; }
        public VirtualProceduresCompanies virtualProcedures { get; set; }
    }
}
