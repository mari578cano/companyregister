﻿using API.CompanyTele.Entities;
using System.Threading.Tasks;

namespace API.CompanyTele.IRepository
{
    public interface IVirtualRegisterCompanyRepository
    {
        Task<bool> SaveRegisterVirtualProcessCompany(Company company);
    }
}
