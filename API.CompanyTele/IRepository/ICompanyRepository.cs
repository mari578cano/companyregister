﻿using API.CompanyTele.Dto;
using API.CompanyTele.Entities;
using System.Threading.Tasks;

namespace API.CompanyTele.IRepository
{
    public interface ICompanyRepository
    {
        Task<CompanyRegister> SearchNit(long nit);
        Task<Company> SaveCompany(Company company);
        bool UpdateCompany(Company company);
    }
}
