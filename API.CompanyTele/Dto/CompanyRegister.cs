﻿using API.CompanyTele.Entities;
using System;
using System.Collections.Generic;

namespace API.CompanyTele.Dto
{
    public class CompanyRegister
    {
        public int Id { get; set; }
        public long NIT { get; set; }
        public int IdType { get; set; }
        public string NameCompany { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public bool AuthorizeSendMessagePhone { get; set; }
        public bool AuthorizeSendMessageEmail { get; set; }
        public DateTime Created { get; set; }
        public VirtualProceduresCompanies virtualProcedures { get; set; }
    }
}
