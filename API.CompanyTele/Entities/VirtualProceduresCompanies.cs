﻿using System;

namespace API.CompanyTele.Entities
{
    public class VirtualProceduresCompanies
    {
        public int Id { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
    }
}
