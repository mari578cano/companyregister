﻿using API.CompanyTele.Data;
using API.CompanyTele.Entities;
using API.CompanyTele.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.CompanyTele.Repository
{
    public class VirtualRegisterCompanyRepository : IVirtualRegisterCompanyRepository
    {
        private readonly DataContext _context;
        private readonly ICompanyRepository _companyRepository;
        public VirtualRegisterCompanyRepository(DataContext context, ICompanyRepository companyRepository)
        {
            _context = context;
            _companyRepository = companyRepository;
        }

        public async Task<bool> SaveRegisterVirtualProcessCompany(Company company)
        {
            using var transaction = _context.Database.BeginTransaction();
            try
            {
                _companyRepository.UpdateCompany(company);
                bool SearchRegisterVirtualProcess = _context.VirtualProceduresCompanies.AsNoTracking().Where(x => x.CompanyId == company.Id).FirstOrDefault().IsActive;

                if(!SearchRegisterVirtualProcess)
                    SaveVirtualProcess(company.Id);

                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        private bool SaveVirtualProcess(int CompanyId) {
            try
            {
                VirtualProceduresCompanies objVirtualProcess = new VirtualProceduresCompanies();
                objVirtualProcess.CompanyId = CompanyId;
                objVirtualProcess.DateAdded = DateTime.Now;
                objVirtualProcess.IsActive = true;
                _context.VirtualProceduresCompanies.Add(objVirtualProcess);
                _context.SaveChanges();

                return true;
            }
            catch (Exception e) {
                return false;
            }
        }

    }
}
