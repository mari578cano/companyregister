﻿using API.CompanyTele.Data;
using API.CompanyTele.Entities;
using API.CompanyTele.IRepository;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using API.CompanyTele.Dto;
using System.Linq;

namespace API.CompanyTele.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly DataContext _context;
        public CompanyRepository(DataContext context) {
            _context = context;
        }

        public async Task<Company> SaveCompany(Company company)
        {
            try
            {
                var existCompany = _context.Company.AsNoTracking().Where(x => x.NIT == company.NIT).FirstOrDefault();
                if (existCompany is null) {
                    _context.Company.Add(company);
                    await _context.SaveChangesAsync();
                    return company;
                }

                return null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateCompany(Company company)
        {
            try
            {
                int id = _context.Company.AsNoTracking().Where(x => x.NIT == company.NIT).FirstOrDefault().Id;
                company.Id = id;
                _context.Company.Update(company);
                _context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<CompanyRegister> SearchNit(long nit)
        {
            try
            {
                var result = await ConsumeServiceValidateCompany(nit);

                if (result.NIT > 0 && result.NIT == nit)
                    return result;
                else
                    return result;
            }
            catch (Exception e) { 
                throw e;
            }
        }

        private async Task<CompanyRegister> ConsumeServiceValidateCompany(long nit) {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    CompanyRegister companyDeserealize = new CompanyRegister();
                    client.BaseAddress = new Uri("https://localhost:44324/");
                    HttpResponseMessage response = await client.GetAsync(client.BaseAddress + "ValidateCompany/validate/" + nit);
                    if (response.IsSuccessStatusCode)
                    {
                        string result = await response.Content.ReadAsStringAsync();
                        companyDeserealize = JsonConvert.DeserializeObject<CompanyRegister>(result);
                        return companyDeserealize;
                    }
                    else
                        return companyDeserealize;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
