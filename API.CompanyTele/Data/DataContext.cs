﻿using API.CompanyTele.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.CompanyTele.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options) {
        }

        public DbSet<Company> Company { get; set; }
        public DbSet<VirtualProceduresCompanies> VirtualProceduresCompanies { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Company>()
        //        .HasOne(a => a.virtualProcedures)
        //        .WithOne(b => b.Company)
        //        .HasForeignKey<VirtualProceduresCompanies>(b => b.CompanyId);
        //}
    }
}
