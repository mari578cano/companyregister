﻿using API.CompanyTele.Dto;
using API.CompanyTele.Entities;
using API.CompanyTele.IRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.CompanyTele.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository _company;

        public CompanyController(ICompanyRepository company)
        {
            _company = company;
        }

        [HttpGet("validate/{nit}")]
        public async Task<ActionResult<CompanyRegister>> ValidateCompany(long nit)
        {
            CompanyRegister companyResult = await _company.SearchNit(nit);

            if (companyResult.Id > 0)
                return Ok(companyResult);
            else
                return BadRequest(companyResult);
        }

        [HttpPost]
        public async Task<ActionResult<bool>> SaveCompany(Company company)
        {
            var result = await _company.SaveCompany(company);

            if(result is null)
                return BadRequest(); 
            else
                return Ok();
        }

    }
}
