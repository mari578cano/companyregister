﻿using API.CompanyTele.Entities;
using API.CompanyTele.IRepository;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.CompanyTele.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VirtualRegisterCompanyController : Controller
    {
        private readonly IVirtualRegisterCompanyRepository _virtualProcedureCompany;
        public VirtualRegisterCompanyController(IVirtualRegisterCompanyRepository virtualProcedureCompany)
        {
            _virtualProcedureCompany = virtualProcedureCompany;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> RegisterServiceCompany(Company company)
        {
            var result = await _virtualProcedureCompany.SaveRegisterVirtualProcessCompany(company);

            if (result)
                return Ok();
            else
                return BadRequest();
        }
    }
}
